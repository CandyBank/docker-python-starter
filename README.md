# Docker Python Starter project on Ubuntu

Used for python-based development @lumiolabs.

## Building the image

```sh
docker build -t python-starter-ubuntu .
```

## Versioning

The `version.txt` is automatically incremented (patch only) on every push (to repo) using the `increment_version.sh` file.
This is achieved with a pre-commit hook. More info [here](https://stackoverflow.com/a/17101505).

Git tag is also added to every git push with the version number.
This is achieved with a pre-push hook. More info [here](https://stackoverflow.com/a/36790066).

Make sure all script files are executable:

```sh
chmod +x .git/hooks/pre-commit
chmod +x .git/hooks/pre-push
```

Do not forget to push tags also! The following command might come handy:

```sh
git config --global push.followTags true
```

## Deploying the image

The image is deployed automatically by BitBucket pipelines on a master merge or commit.

_coded with ❤️ @lumiolabs_
