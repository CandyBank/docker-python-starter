#!/bin/sh
APPVER=$(cat version.txt)
NEWVER="${APPVER%.*}.$((${APPVER##*.}+1))"
echo $NEWVER > ./version.txt

git add ./version.txt
