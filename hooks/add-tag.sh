#!/bin/sh

# get the last commit Id
LAST_COMMIT=$(git log --format="%H" -1 HEAD)
APPVER=$(cat version.txt)

tag="v$APPVER"
msg="$tag"

if [ ! $(git tag -l "$tag") ]; then
  git tag -a $tag $LAST_COMMIT -m $msg
fi

