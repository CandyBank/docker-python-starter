FROM ubuntu:18.04

LABEL maintainer="lumio labs <lumiolabs@lumiolabs.io>" \
      name="lumio Python Container" \
      description="Ubuntu for app development with Python" \
      url="https://hub.docker.com/r/lumiolabs/python-starter-ubuntu" \
      vendor="lumio labs" \
      version="1.0"

ENV VIRTUAL_ENV=/opt/venv \
    PATH="$VIRTUAL_ENV/bin:$PATH"
    # HOME=/opt/code

# Expose port
EXPOSE 5000

# Create working (/opt/code) and temp (/opt/code/tmp) folders => `-p` flag creates parent directories also
RUN mkdir -p /opt/code/tmp /opt/venv

# Set working directory
WORKDIR /opt/code/

# Copy code
ADD ./requirements.txt /opt/code/

# Install some basic utilities
RUN apt-get update && apt-get install -y --no-install-recommends \
        curl \
        nano \
        ca-certificates \
        sudo \
        git \
        bzip2 \
        libx11-6 \
        netcat \
        postgresql-client \
        # build tools
        libpq-dev build-essential libssl-dev libffi-dev python3-dev \
        # python
        python3.7 \
        python3-pip \
        python3-setuptools \
        python3-virtualenv \
    && rm -rf /var/lib/apt/lists/* \
    # Create a non-root user and switch to it
    # && adduser --disabled-password --gecos '' --shell /bin/bash user \
    #     && chown -R user:user /opt/code \
    # && echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user \
    # All users can use /home/user as their home directory
    # && chmod 777 /opt/code \
    # Grant access to virtualenv root folder
    # && chmod 777 /opt/venv \
    # create virtual env
    && python3 -m virtualenv --python=/usr/bin/python3 $VIRTUAL_ENV \
    # install dependencies
    && pip3 install -r requirements.txt \
    && rm -rf requirements.txt

# USER user

# Set the default command to python3
CMD ["python3"]